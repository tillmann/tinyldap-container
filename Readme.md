# tinyldap Container

Builds a minimal image containing [tinyldap](http://www.fefe.de/tinyldap/).
I used this as an excercise while searching for a lean and minimal Idm
solution. I don't think tinyldap is a viable path.

This is just here for completeness.

```bash
podman build --manifest tinyldap -f Containerfile
podman run -v ./exp.ldif:/exp.ldif:z -p 8389:389 localhost/tinyldap
```
